// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Types shared between multiple commands.

use std::ffi::{OsStr, OsString};
use std::path::PathBuf;
use std::process::{Command, Stdio};

use structopt::clap::arg_enum;
use structopt::StructOpt;

use crate::Error;

/// Same as [`vec!`] but calls [`AsRef::as_ref`] on all elements.
///
/// # Examples
/// ```
/// let kernel: &OsStr = "target/rost".as_ref();
///
/// let v: Vec<&OsStr> = vec_osstr![
///     "qemu-system-riscv32",
///     "-kernel", kernel,
/// ];
/// ```
///
/// [`vec!`]: https://doc.rust-lang.org/std/macro.vec.html
/// [`AsRef::as_Ref`]: https://doc.rust-lang.org/std/convert/trait.AsRef.html
#[macro_export]
macro_rules! vec_osstr {
    ($($x:expr),+ $(,)?) => {
        vec![$($x.as_ref()),+];
    }
}

/// Same as [`vec!`] and [`vec_osstr!`] but calls [`ToOwned::to_owned`] on all
/// elements.
///
/// # Examples
/// See [`vec_osstr!`].
///
/// [`vec!`]: https://doc.rust-lang.org/std/macro.vec.html
/// [`ToOwned::to_owned`]: https://doc.rust-lang.org/std/borrow/trait.ToOwned.html
/// [`vec_osstr!`]: vec_osstr
#[macro_export]
macro_rules! vec_owned {
    ($($x:expr),+ $(,)?) => {
        vec![$($x.to_owned()),+];
    }
}

/// Same as [`vec!`] and [`vec_osstr!`] but calls [`OsString::from`] on all
/// elements.
///
/// # Examples
/// See [`vec_osstr!`].
///
/// [`vec!`]: https://doc.rust-lang.org/std/macro.vec.html
/// [`OsString::from`]: https://doc.rust-lang.org/std/ffi/struct.OsString.html
/// [`vec_osstr!`]: vec_osstr
#[macro_export]
macro_rules! vec_osstring {
    ($($x:expr),+ $(,)?) => {
        vec![$(OsString::from($x)),+];
    }
}

macro_rules! push_osstrings {
    ($v:expr, $($x:expr),+ $(,)?) => {
        let v = &mut $v;
        $(v.push(OsString::from($x));)+
    };
}

/// Does nothing. Only acts as a marker for rustfmt not to mess with the
/// formatting.
#[macro_export]
macro_rules! args {
    ($($x:expr),+ $(,)?) => {
        [$($x),+];
    }
}

/// Joins to strings into an [`OsString`].
///
/// [`OsString`]: https://doc.rust-lang.org/std/ffi/struct.OsString.html
pub fn join_osstr<S1: AsRef<OsStr>, S2: AsRef<OsStr>>(s1: S1, s2: S2) -> OsString {
    let a = s1.as_ref();
    let b = s2.as_ref();

    let mut s = OsString::with_capacity(a.len() + b.len());
    s.push(a);
    s.push(b);
    s
}

arg_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum Board {
        Qemu,
    }
}

impl Board {
    /// Cargo feature name for this board.
    pub const fn feature(&self) -> &str {
        match self {
            Board::Qemu => "board/board-qemu",
        }
    }
}

#[rustfmt::skip::macros(vec_osstr)]
impl Board {
    /// Returns a [`Vec<&str>`][Vec] that can be passed to [`Command::args`] to
    /// start an emulator that listens on `tcp:1234` for a GDB connection.
    ///
    /// [Vec]: https://doc.rust-lang.org/std/vec/struct.Vec.html
    /// [`Command::args`]: https://doc.rust-lang.org/std/process/struct.Command.html#method.args
    pub fn emulate(self, kernel: &OsStr) -> Vec<&OsStr> {
        match self {
            Board::Qemu => vec_osstr![
                "qemu-system-riscv32",
                "-machine", "virt",
                "-cpu", "any",
                "-smp", "1",
                "-m", "128M",
                "-nographic",
                "-serial", "mon:stdio",
                "-bios", "none",
                "-s", "-S",
                "-kernel", kernel,
            ],
        }
    }
}

/// Options related to compiling.
#[derive(StructOpt, Debug)]
pub struct CompileOptions {
    /// Board to compile for.
    #[structopt(short, long, default_value = "QEMU")]
    pub board: Board,

    /// Pass `--release` to cargo.
    #[structopt(short, long)]
    pub release: bool,

    /// Target triplet to compile for.
    #[structopt(long, default_value = "riscv32imac-unknown-none-elf")]
    pub target: String,
}

#[rustfmt::skip::macros(vec)]
impl CompileOptions {
    /// Run `cargo` to build the given project.
    pub fn compile_project(&self, project: &str) -> Result<bool, Error> {
        let mut opts = vec![
            "build",
            "--color", "always",
            "--package", project,
            "--features", self.board.feature(),
            "--target", &self.target,
            "-Zextra-link-arg",
        ];
        if self.release {
            opts.push("--release");
        }
        let success = Command::new("cargo")
            .args(&opts)
            .stdin(Stdio::null())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .status()?
            .success();
        Ok(success)
    }

    /// Return the path of the executable.
    ///
    /// # Examples
    /// ```
    /// let compile_options = CompileOptions {
    ///     board: Board::Qemu,
    ///     release: true,
    ///     target: "riscv32imac-unknown-none-elf".to_owned(),
    /// };
    ///
    /// assert_eq!(compile_options.exe_path("memory").to_str().unwrap(), "target/riscv32imac-unknown-none-elf/release/memory");
    /// ```
    pub fn exe_path(&self, exe: &str) -> PathBuf {
        let mut path = PathBuf::new();
        path.push("target");
        path.push(&self.target);
        path.push(if self.release { "release" } else { "debug" });
        path.push(exe);
        path
    }
}
