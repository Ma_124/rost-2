// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Build, debug and lint utility for all crates in this workspace.

#![feature(external_doc)]

use std::ffi::OsString;
use std::io;
use std::process::exit;

use err_derive::Error;
use structopt::StructOpt;

use crate::debug::Debug;
use crate::doc::Doc;
use crate::lint::Lint;
use crate::test_board::TestBoard;

#[macro_use]
mod common;
mod debug;
mod doc;
mod lint;
mod test_board;

#[cfg(doc)]
#[doc(include = "../../README.md")]
pub struct Readme;

/// Represents an error.
#[derive(Error, Debug)]
pub enum Error {
    /// Wrapper around io::Error.
    #[error(display = "io error: {}", _0)]
    IO(#[source] io::Error),

    /// Executable not found.
    #[error(display = "executable not found: {:?}", _0)]
    ExeNotFound(OsString),

    /// Sysroot not found.
    #[error(display = "executable not found")]
    SysrootNotFound,
}

/// Build, debug and lint utility for all crates in this workspace.
#[derive(StructOpt, Debug)]
enum Cli {
    /// Call clippy and rustfmt.
    Lint(Lint),

    /// Run the project in an emulator and attach GDB.
    Debug(Debug),

    /// Run a board test in an emulator and check the result.
    TestBoard(TestBoard),

    /// Build documentation.
    Doc(Doc),
}

fn main() {
    let cli: Cli = Cli::from_args();
    match match cli {
        Cli::Lint(l) => l.main(),
        Cli::Debug(d) => d.main(),
        Cli::TestBoard(b) => b.main(),
        Cli::Doc(d) => d.main(),
    } {
        Ok(status) => exit(status),
        Err(e) => {
            println!("{}", e);
            exit(2);
        }
    }
}
