// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Provides the `test-board` subcommand.

use std::ffi::OsString;
use std::fs;
use std::io::{ErrorKind, Read};
use std::path::{Path, PathBuf};

use structopt::StructOpt;
use tempfile::NamedTempFile;

use crate::common::CompileOptions;
use crate::debug::{Debug, GdbOptions};
use crate::Error;

/// Run a board test in an emulator and check the result.
#[derive(StructOpt, Debug)]
pub struct TestBoard {
    /// Test to run. (e.g. `memory`)
    #[structopt(name = "TEST")]
    pub test: String,

    /// Don't run in batch mode and don't source ~/.gdbinit.
    ///
    /// Takes precedence over `--batch`.
    #[structopt(short = "T", long)]
    pub no_batch: bool,

    #[structopt(flatten)]
    #[allow(clippy::missing_docs_in_private_items)]
    pub compile_options: CompileOptions,

    #[structopt(flatten)]
    #[allow(clippy::missing_docs_in_private_items)]
    pub gdb_options: GdbOptions,
}

#[rustfmt::skip::macros(push_osstrings)]
impl TestBoard {
    /// Implementation of `test-board` command.
    pub fn main(mut self) -> Result<i32, Error> {
        let mut stdout = NamedTempFile::new()?;
        let mut gdbout = NamedTempFile::new()?;

        let dir = PathBuf::from(format!("board_tests/src/bin/{}/test.gdb", self.test));

        if self.gdb_options.script.is_none() {
            let script_path = dir.clone();
            if script_path.exists() {
                self.gdb_options.script = Some(script_path.into_os_string());
            } else {
                push_osstrings!(
                    self.gdb_options.additional_gdb_options,
                    "-ex", "c",
                    "-ex", "q"
                );
            }
        }

        if self.gdb_options.script.is_some() {
            push_osstrings!(
                self.gdb_options.additional_gdb_options,
                "-ex", "set logging overwrite",
                "-ex", format!("set logging file {}", gdbout.path().to_str().unwrap())
            );
        }

        self.gdb_options.batch = !self.no_batch;

        let debug = Debug {
            project: "board_tests".to_owned(),
            exe: Some(self.test.clone()),
            stdout: stdout.path().to_owned().into_os_string(),
            compile_options: self.compile_options,
            gdb_options: self.gdb_options,
        };

        let success = debug.main()?;
        if success != 0 {
            return Ok(success);
        }

        if print_diff(&dir, &mut stdout, "stdout")? && print_diff(&dir, &mut gdbout, "gdbout")? {
            Ok(0)
        } else {
            Ok(1)
        }
    }
}

/// Returns the contents of the file at the given path or an empty string if it
/// doesn't exist.
fn read_or_empty(p: &Path) -> Result<String, Error> {
    match fs::read_to_string(p) {
        Ok(v) => Ok(v),
        Err(err) => match err.kind() {
            ErrorKind::NotFound => Ok("".to_owned()),
            _ => Err(err.into()),
        },
    }
}

/// Prints a diff between the files `got_file` and `with_ext(want_path,
/// suffix)`.
fn print_diff(dir: &Path, got_file: &mut NamedTempFile, suffix: &str) -> Result<bool, Error> {
    let mut got = String::new();
    got_file.read_to_string(&mut got)?;

    let want_path = dir.with_file_name(suffix);
    let want = read_or_empty(&want_path)?;

    if got != want {
        print!(
            "\n{}",
            similar::TextDiff::from_lines(&want, &got)
                .unified_diff()
                .header(want_path.to_str().unwrap(), &format!("got.{}", suffix))
                .missing_newline_hint(true)
        );
        Ok(false)
    } else {
        println!("{}:\n{}", suffix, got);
        Ok(true)
    }
}
