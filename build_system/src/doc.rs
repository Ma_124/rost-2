// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Provides the `doc` subcommand.

use std::fs;
use std::process::{Command, Stdio};

use structopt::StructOpt;

use crate::common::Board;
use crate::Error;

/// An HTML file that immediately redirects to [`build_system::Readme`].
///
/// [`build_system::Readme`]: crate::Readme
static REDIRECT_HTML: &str = r#"<html><head><meta charset="utf-8"><meta http-equiv="refresh" content="0;url=./build_system/struct.Readme.html"><style>body{color:#fff;background-color:#0f1419;}</style></head><body><a href="./build_system/struct.README.html">Redirecting</a></body></html>"#;

/// Build documentation.
#[derive(StructOpt, Debug)]
pub struct Doc {}

#[rustfmt::skip::macros(args)]
impl Doc {
    /// Implementation of `doc` command.
    pub fn main(&self) -> Result<i32, Error> {
        let success = Command::new("cargo")
            .args(&args![
                "doc",
                "--color", "always",
                "--all",
                "--release",
                "--features", Board::Qemu.feature(),
                "--document-private-items",
                "-Zextra-link-arg",
            ])
            .stdin(Stdio::null())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .status()?
            .success();

        fs::write("target/doc/index.html", REDIRECT_HTML)?;

        if success {
            Ok(0)
        } else {
            Ok(1)
        }
    }
}
