// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Provides the `lint` subcommand which calls [clippy] and [rustfmt].
//!
//! [clippy]: https://github.com/rust-lang/rust-clippy/
//! [rustfmt]: https://github.com/rust-lang/rustfmt/

use std::process::{Command, Stdio};

use structopt::StructOpt;

use crate::common::Board;
use crate::Error;

/// Call clippy and rustfmt.
#[derive(StructOpt, Debug)]
pub struct Lint {
    /// Fix all formatting issues.
    #[structopt(short, long)]
    fix: bool,

    /// Board to run clippy for.
    #[structopt(short, long, default_value = "QEMU")]
    board: Board,
}

#[rustfmt::skip::macros(vec, args)]
impl Lint {
    #[allow(clippy::too_many_lines)]
    /// Implementation of `lint` command.
    pub fn main(&self) -> Result<i32, Error> {
        let mut success = Command::new("cargo")
            .args(&args![
                "clippy",
                "--color", "always",
                "--features", self.board.feature(),
                "-Zextra-link-arg",
                "--",
                "-W", "clippy::cognitive_complexity",
                "-W", "clippy::missing_docs_in_private_items",
                "-W", "clippy::missing_panics_doc",
                "-W", "clippy::missing_const_for_fn",
                "-W", "clippy::needless_pass_by_value",
                "-W", "clippy::semicolon_if_nothing_returned",
                "-W", "clippy::single_match_else",
                "-W", "clippy::string_lit_as_bytes",
                "-W", "clippy::string_to_string",
                "-W", "clippy::str_to_string",
                "-W", "clippy::suboptimal_flops",
                "-W", "clippy::too_many_lines",
                "-W", "clippy::trivially_copy_pass_by_ref",
                "-W", "clippy::unicode_not_nfc",
                "-W", "clippy::unseparated_literal_suffix",
                "-D", "clippy::wildcard_dependencies",
            ])
            .stdin(Stdio::null())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .status()?
            .success();

        let mut fmt_opts = vec![
            "--color", "always",
            "fmt",
            "--all",
            "--",
            "--color", "always",
        ];

        if !self.fix {
            fmt_opts.push("--check");
        }

        success &= Command::new("cargo")
            .args(&fmt_opts)
            .stdin(Stdio::null())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .status()?
            .success();

        if success {
            Ok(0)
        } else {
            Ok(1)
        }
    }
}
