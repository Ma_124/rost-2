// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Provides the `debug` subcommand.

use std::env;
use std::ffi::{OsStr, OsString};
use std::fs::File;
use std::os::unix::ffi::OsStrExt;
use std::process::{Command, Stdio};

use structopt::StructOpt;

use crate::common::{join_osstr, CompileOptions};
use crate::Error;

/// Run the project in an emulator and attach GDB.
#[derive(StructOpt, Debug)]
pub struct Debug {
    /// Project to compile. (e.g. `board_tests`)
    #[structopt(name = "PROJECT")]
    pub project: String,

    /// Binary in project to run. (e.g. `memory`)
    #[structopt(name = "BIN")]
    pub exe: Option<String>,

    /// Stdout for the emulator.
    #[structopt(short = "o", long, default_value = "/dev/null")]
    pub stdout: OsString,

    #[structopt(flatten)]
    #[allow(clippy::missing_docs_in_private_items)]
    pub compile_options: CompileOptions,

    #[structopt(flatten)]
    #[allow(clippy::missing_docs_in_private_items)]
    pub gdb_options: GdbOptions,
}

/// General options to launch GDB.
#[derive(StructOpt, Debug)]
pub struct GdbOptions {
    /// GDB executable to use.
    #[structopt(long, default_value = "riscv64-elf-gdb")]
    pub gdb: OsString,

    /// GDB script to execute.
    #[structopt(long)]
    pub script: Option<OsString>,

    /// Run in batch mode and don't source ~/.gdbinit.
    #[structopt(short = "t", long)]
    pub batch: bool,

    /// Additional options to pass to GDB
    #[structopt(skip)]
    pub additional_gdb_options: Vec<OsString>,
}

impl Debug {
    /// Implementation of `debug` command.
    pub fn main(&self) -> Result<i32, Error> {
        // `cargo build`
        if !self.compile_options.compile_project(&self.project)? {
            return Ok(1);
        }

        let exe_path = self.compile_options.exe_path(if let Some(p) = &self.exe {
            p
        } else {
            &self.project
        });
        if !exe_path.exists() {
            return Err(Error::ExeNotFound(exe_path.into()));
        }

        // start emulator
        let emu_opts: Vec<&OsStr> = self.compile_options.board.emulate(exe_path.as_os_str());
        let mut emu = Command::new(&emu_opts[0])
            .args(&emu_opts[1..])
            .stdin(Stdio::null())
            .stdout(Stdio::from(File::create(self.stdout.clone())?))
            .stderr(Stdio::null())
            .spawn()?;

        // run gdb
        let success = gdb(exe_path.as_ref(), &self.gdb_options);

        // kill emulator
        emu.kill()?;

        // check for errors while running gdb
        if success? {
            Ok(0)
        } else {
            Ok(1)
        }
    }
}

/// Run GDB on executable `exe` with [`GdbOptions`]
///
/// [`GdbOptions`]: GdbOptions
#[rustfmt::skip::macros(vec_osstr)]
fn gdb(exe: &OsStr, gdb_opts: &GdbOptions) -> Result<bool, Error> {
    let sysroot = rustc_sysroot()?;
    let gdb_python_path = join_osstr(sysroot, "/lib/rustlib/etc");

    let mut python_path = env::var_os("PYTHONPATH").unwrap_or_else(OsString::new);
    if python_path.len() != 0 {
        python_path.push(":");
    }
    python_path.push(&gdb_python_path);

    let iex = join_osstr("add-auto-load-safe-path ", &gdb_python_path);

    let mut opts: Vec<&OsStr> = vec_osstr![
        "-q",
        "--directory", &gdb_python_path,
        "-iex", &iex,
        "-ex", "target remote localhost:1234",
        "-ex", "tbreak park",
        "-ex", "tbreak kmain",
        "-ex", "continue",
        exe,
    ];

    if gdb_opts.batch {
        opts.push("--batch-silent".as_ref());
        opts.push("-nh".as_ref());
    }

    for opt in &gdb_opts.additional_gdb_options {
        opts.push(opt.as_ref());
    }

    if let Some(script) = &gdb_opts.script {
        opts.push("-x".as_ref());
        opts.push(script.as_ref());
    }

    Ok(Command::new(&gdb_opts.gdb)
        .args(&opts)
        .env("PYTHONPATH", python_path)
        .stdin(if gdb_opts.batch {
            Stdio::null()
        } else {
            Stdio::inherit()
        })
        .status()?
        .success())
}

/// Runs `rustc --print=sysroot`.
fn rustc_sysroot() -> Result<OsString, Error> {
    let mut sysroot = Command::new("rustc")
        .args(&["--print=sysroot"])
        .stdout(Stdio::piped())
        .output()?;

    while let Some(b'\n') | Some(b'\r') = sysroot.stdout.last() {
        sysroot.stdout.pop();
    }

    if !sysroot.status.success() && (sysroot.stdout.is_empty()) {
        return Err(Error::SysrootNotFound);
    }

    Ok(OsStr::from_bytes(&sysroot.stdout).to_owned())
}
