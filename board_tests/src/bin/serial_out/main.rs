// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]
#![no_main]
#![feature(lang_items)]
#![allow(clippy::single_component_path_imports)]

//! Tests basic memory interactions on the target board.
//!
//! Can be tested with:
//! ```sh
//! ./run test-board -r serial_out
//! ```

use core::fmt::Write;
use core::panic::PanicInfo;

use board;
use board::{Board, SerialOut, SomeBoard};

/// Handles panics.
#[panic_handler]
fn panic_handler(_: &PanicInfo) -> ! {
    unsafe {
        Board::playground_addr().add(0).write_volatile(0x50);
        Board::park()
    }
}

#[lang = "eh_personality"]
#[no_mangle]
#[allow(clippy::missing_const_for_fn)]
extern "C" fn eh_personality() {}

/// Kernel entrypoint called from `kstart`.
#[no_mangle]
extern "C" fn kmain() {
    let mut serial = unsafe { Board::unsafe_serial_out() };
    serial.reset().expect("reset serial");
    serial.write_str("Hello World!\n").expect("write");
}
