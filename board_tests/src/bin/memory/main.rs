// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]
#![no_main]
#![feature(lang_items)]
#![allow(clippy::single_component_path_imports)]

//! Tests basic memory interactions on the target board.
//!
//! Can be tested with:
//! ```sh
//! ./run test-board -r memory
//! ```

use core::panic::PanicInfo;

use board;
use board::{Board, SomeBoard};

/// Handles panics.
#[panic_handler]
fn panic_handler(_: &PanicInfo) -> ! {
    unsafe {
        Board::playground_addr().add(1).write_volatile(0x50);
        Board::park()
    }
}

#[lang = "eh_personality"]
#[no_mangle]
#[allow(clippy::missing_const_for_fn)]
extern "C" fn eh_personality() {}

/// Kernel entrypoint called from `kstart`.
#[no_mangle]
extern "C" fn kmain() {
    unsafe {
        let base_addr = Board::playground_addr();

        base_addr.add(1).write_volatile(0x59);
        base_addr
            .add(2)
            .write_volatile(base_addr.add(0).read_volatile());

        Board::park()
    }
}
