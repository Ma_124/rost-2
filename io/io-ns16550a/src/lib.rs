// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]

//! Implements drivers for the NS16550A UART chip.

use board_info::SerialOut;

/// Driver for NS16550A UART chip.
#[derive(Copy, Clone, Debug)]
pub struct NS16550a {
    /// Address of the memory-mapped io chip.
    pub addr: *mut u8,
}

/// Initializes the UART chip for normal operation.
///
/// The serial connection will have these settings:
/// - Baud:          115200
/// - Parity:        None
/// - Data bits:     8
/// - Stop bits:     1
/// - Hardware Flow: None
#[allow(clippy::identity_op)]
impl SerialOut for NS16550a {
    type Error = ();

    fn reset(&mut self) -> Result<(), Self::Error> {
        unsafe {
            // write Interrupt Enable Register `ier`
            // 0                  disable interrupts
            let ier: u8 = 0;
            self.addr.add(1).write_volatile(ier);

            // write FIFO Control Register `fcr`
            // (       0b1)  << 0 Enable FIFOs
            let fcr: u8 = (0b1 << 0) | 0;
            self.addr.add(2).write_volatile(fcr);

            // write Line Control Register `lcr`
            // (wls  = 0b11) << 0 8-bit word length
            // (stb  = 0b0)  << 2 Number of stop bits (0b0 -> 1, 0b1 -> 2)
            // (dlab = 0b1)  << 7 (0)addr and (1)addr now access the divisor
            // 0                  disable parities
            let lcr: u8 = (0b11 << 0) | (0b0 << 2);
            self.addr.add(3).write_volatile(lcr | (1 << 7));

            let div = 592_u16;
            self.addr.add(0).write_volatile((div & 0xFF) as u8);
            self.addr.add(1).write_volatile((div >> 8) as u8);

            // (dlab = 0b0)  << 7 (0)addr and (1)addr now access receive/transmit
            self.addr.add(3).write_volatile(lcr);
        }
        Ok(())
    }

    fn write_byte(&mut self, b: u8) -> Result<(), Self::Error> {
        unsafe {
            self.addr.add(0).write_volatile(b);
        }
        Ok(())
    }
}

impl core::fmt::Write for NS16550a {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        if self.write(s).is_ok() {
            Ok(())
        } else {
            Err(core::fmt::Error)
        }
    }
}
