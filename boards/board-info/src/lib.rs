// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]
#![feature(doc_notable_trait)]

//! Generic traits implemented by board support packages.
//!
//! Only `board`, drivers and board support packages should directly depend on
//! this crate. For all other purposes reexports `board` the contained types.

/// Board description implemented by board support packages.
#[doc(notable_trait)]
pub trait SomeBoard: Sized {
    /// Type of main serial output device
    type SerialOut: SerialOut;

    /// An address in RAM that is likely to be unused and has some free bytes
    /// after it.
    ///
    /// # Safety
    /// It is not guaranteed that the returned address is unused.
    unsafe fn playground_addr() -> *mut u8;

    /// Initialize board for normal use.
    fn init() -> Self;

    /// Returns the main serial output device in an unknown state.
    ///
    /// # Safety
    /// This function is inherently unsafe because the chip is in some unknown
    /// state and might already be in use.
    unsafe fn unsafe_serial_out() -> Self::SerialOut;

    /// Suspends current core.
    ///
    /// # Safety
    /// Immediately stops core without running destructors.
    unsafe fn park() -> !;
}

/// A serial output device.
#[doc(notable_trait)]
pub trait SerialOut: Sized + core::fmt::Write {
    /// Represents io errors by this device.
    type Error: core::fmt::Debug;

    /// Resets or initializes this device.
    fn reset(&mut self) -> Result<(), Self::Error>;

    /// Writes one byte.
    fn write_byte(&mut self, b: u8) -> Result<(), Self::Error>;

    /// Writes multiple bytes.
    ///
    /// Implementations should override this if they can accept more than one
    /// byte.
    fn write_bytes(&mut self, bs: &[u8]) -> Result<(), Self::Error> {
        for b in bs {
            self.write_byte(*b)?;
        }

        Ok(())
    }

    /// Writes a string.
    fn write<S: AsRef<str>>(&mut self, s: S) -> Result<(), Self::Error> {
        self.write_bytes(s.as_ref().as_bytes())
    }
}
