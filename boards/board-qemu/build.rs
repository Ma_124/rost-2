// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

//! Build script for `board-qemu`.

fn main() {
    println!("cargo:rustc-link-arg=-Tboards/board-qemu/default.lds");
}
