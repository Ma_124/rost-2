// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]
#![feature(global_asm)]
#![feature(asm)]

//! Board support package for [QEMU]'s [`virt`] device.
//!
//! [QEMU]: https://www.qemu.org/
//! [`virt`]: https://github.com/qemu/qemu/blob/master/hw/riscv/virt.c

use core::hint::unreachable_unchecked;

use board_info::SomeBoard;
use io_ns16550a::NS16550a;

/// Singleton QEMU board instance.
#[derive(Copy, Clone)]
pub struct QEMU;

impl SomeBoard for QEMU {
    type SerialOut = NS16550a;

    unsafe fn playground_addr() -> *mut u8 {
        (0x80000000_usize + 127 * 1000000) as *mut u8
    }

    fn init() -> Self {
        Self
    }

    /// Returns [UART0].
    ///
    /// [UART0]: https://github.com/qemu/qemu/blob/a97978bcc2d1f650c7d411428806e5b03082b8c7/hw/riscv/virt.c#L54
    unsafe fn unsafe_serial_out() -> Self::SerialOut {
        NS16550a {
            addr: 0x10000000 as *mut u8,
        }
    }

    unsafe fn park() -> ! {
        asm!("j park");
        unreachable_unchecked()
    }
}

global_asm!(include_str!("boot.S"));
