// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
// SPDX-License-Identifier: BSD-2-Clause

#![no_std]

//! Information about the current board.

pub use board_info::*;
#[cfg(feature = "board-qemu")]
pub use board_qemu::QEMU as Board;
