# Rost 2
[![Pipeline Status](https://gitlab.com/Ma_124/rost-2/badges/master/pipeline.svg)](https://gitlab.com/Ma_124/rost-2/-/commits/master)
[![Docs](https://img.shields.io/badge/docs-latest-4d76ae)](https://ma_124.gitlab.io/rost-2/build_system/)
[![License: BSD-2-Clause](https://img.shields.io/badge/license-BSD--2--Clause-blue)](https://gitlab.com/Ma_124/rost-2/-/blob/master/LICENSE)

A second attempt at writing an experimental kernel for [RISC-V] in [Rust].

## Setup
Requirements:
- Working [Rust installation].
- GCC for RISC-V (riscv64-elf-gcc)
- GDB for RISC-V (riscv64-elf-gdb)
- QEMU for RISC-V (qemu-arch-extra)

``` sh
git clone https://gitlab.com/Ma_124/rost-2.git && cd rost-2
rustup target add riscv64gc-unknown-none-elf
rustup component add clippy
source ./activate # compiles build system and sets up some aliases
test-board memory
rost --help
```

## Goals
None.
This kernel is never supposed to actually be useful except for learning.

[RISC-V]: https://en.wikipedia.org/wiki/RISC-V
[Rust]: https://rust-lang.org/
[Rust installation]: https://www.rust-lang.org/learn/get-started
